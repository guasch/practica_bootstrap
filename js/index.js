$(document).ready(function () {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $(".carousel").carousel({
      interval: 2000,
    });
  });
  $("#exampleModal").on("show.bs.modal", function (e) {
    console.log(
      "This event fires immediately when the show instance method is called. If caused by a click, the clicked element is available as the relatedTarget property of the event."
    );
    $("#exampleModalBtn").removeClass("btn-defecto");
    $("#exampleModalBtn").addClass("btn-defecto-disabled");
    $("#exampleModalBtn").prop("disabled",true);
  });
  $("#exampleModal").on("shown.bs.modal", function (e) {
    console.log(
      "This event is fired when the modal has been made visible to the user (will wait for CSS transitions to complete). If caused by a click, the clicked element is available as the relatedTarget property of the event."
    );
  });
  $("#exampleModal").on("hide.bs.modal", function (e) {
    console.log(
      "This event is fired immediately when the hide instance method has been called."
    );
  
  });
  $("#exampleModal").on("hidden.bs.modal", function (e) {
    console.log(
      "This event is fired when the modal has finished being hidden from the user (will wait for CSS transitions to complete)."
    );
    $("#exampleModalBtn").removeClass("btn-defecto-disabled");
    $("#exampleModalBtn").addClass("btn-defecto");
    $("#exampleModalBtn").prop("disabled",false);
  });